PROYECTO 3; 16/10/2019

El proyecto #3 de la materia de Diseño y Análisis de Algoritmos consistió en la construcción 
aleatoria de grafos bajo cuatro diferentes métodos (Erdös y Rényi, Gilbert, Geográfico simple, 
y Barabási-Albert); mismo que fue desarrollado en Java (lo que se presentó en el "Proyecto 1"). 
Utilizando dichos grafos se implementó el algoritmo de Dijkstra de tal forma que dado un nodo 
fuente, se calculó el árbol de caminos más cortos (empleando también un método para asignar 
valores aleatorios a cada arista, dentro de un rango).

A continuación se describirá de manera concisa el procedimiento a realizar para la ejecución
del proyecto:

1.- Abrir en cualquier editor de Java (el proyecto se realizó en NetBeans IDE) el proyecto 
titulado "Proyecto2".
2.- Dentro del proyecto podrán ser visualizadas tres clases: Grafos, Aristas, Nodos. Abrir la
clase "Grafos".
3.- En la clase "Grafos" se encuentra desarrollado el algoritmo pertinente para la generación de
cualquier tipo de grafo (Erdös y Rényi, Gilbert, Geográfico simple, y Barabási-Albert). En la parte
final se puede observar el método "main", donde se da la instrucción de construir el grafo deseado
y colocarle un nombre al archivo .gv.
4.- Una vez generado el grafo dentro de un arvhivo .gv, podrá ser observado de manera gráfica a 
través del software Gephi, en donde se abre el archivo correspondiente y el grafo puede ser 
visualizado.
5.- Al construir cualquier tipo de grafo aleatorio, se le asignarán valores aleatorios como costes
en cada arista mediante el método de "EdgeValues", dando un valor mínimo y uno máximo.
6.- Finalmente se le aplicará el método de Dijkstra a dicho grafo construído, dando un nodo raíz o 
fuente (que para fines prácticos, siempre se escribió que fuera el nodo 0 al generar todos los archivos
del proyecto) y así se obtiene el archivo .gv del árbol de caminos más cortos (mismo que también se
puede visualizar en el software Gephi).
7.- Dentro del archivo .gv generado en el paso anterior, se observan todas las distancias calculadas 
del método Dijkstra, visualizando la distancia mínima (el camino más corto) en el mismo nombre del archivo.

Nota: los métodos para las búsquedas implementadas (BFS y DFS) en el Proyecto 2 también se pueden 
observar dentro del código fuente.

- Viridiana Rodríguez González 